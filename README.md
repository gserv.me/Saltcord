Saltcord
========

A Discord bot designed to provide chatops for sysadmins using Saltstack's 
[rest_cherrypy netapi module](https://docs.saltstack.com/en/latest/ref/netapi/all/salt.netapi.rest_cherrypy.html).

Requirements
------------

We only support running Saltcord as a Docker container, and we provide an automatically-built Docker image at 
`gdude2002/saltcord`. If you're using Docker-Compose (and you should be), you'll find an example `docker-compose.yml`
in the `docker/` directory.

Configuration
-------------

Configuration is primarily handled using environment variables.

### Required

* `BOT_TOKEN`: The Discord bot token to connect with.
* `SALT_PASSWORD`: The password for your Salt admin user.
* `SALT_URL`: The base URL for your Salt REST API instance.
* `SALT_USERNAME`: The username to authenticate with.
* `UPDATES_CHANNEL`: The channel ID to post state running/succeeded/failed statuses to

### Optional

* `DEBUG`: If set, this will result in a far greater amount of logging.
* `SALT_AUTH_TYPE`: The type of authentication to use - either `pam` or `ldap`. Default to `pam`.

Usage
-----

Once you've set up your `docker-compose.yml` with the correct settings, simply `docker-compose up -d` to start the
bot in the background.
