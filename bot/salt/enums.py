from enum import Enum


class AuthTypes(Enum):
    AUTO: str = "auto"
    PAM: str = "pam"
    LDAP: str = "ldap"

    @staticmethod
    def from_string(string) -> "AuthTypes":
        if string == "auto":
            return AuthTypes.AUTO

        elif string == "pam":
            return AuthTypes.PAM

        elif string == "ldap":
            return AuthTypes.LDAP

        raise ValueError(f"Unknown auth type: {string}")
