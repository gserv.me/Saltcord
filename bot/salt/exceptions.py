class SaltException(Exception):
    message = ""

    def __str__(self):
        return self.message

    def __repr__(self):
        return f"<{self.__class__}('message': '{self.message}')>"


class LoginFailedException(SaltException):
    message = "Login failed - Make sure the username, password and auth type are all correct"


class BadRequestException(SaltException):
    message = "Bad request"


class AuthenticationException(SaltException):
    message = "Authentication required or invalid"


class NotFoundException(SaltException):
    message = "Resource not found"


class UnavailableContentTypeException(SaltException):
    message = "Unavailable content-type specified"


class InternalServerErrorException(SaltException):
    message = "Internal server error"


class TimeoutException(SaltException):
    message = "Timed out while waiting for the result of an operation"
