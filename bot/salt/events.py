import json
from logging import getLogger
from typing import Any, Dict

from aiohttp import ClientPayloadError, ClientResponse

HEADERS = {
    "Accept": "application/json",
}

log = getLogger(__name__)


def _headers(**kwargs):
    if kwargs:
        return {**kwargs, **HEADERS}
    return {**HEADERS}


class EventStream:
    _continue: bool = None
    _event: Dict[str, Any] = None
    _response: ClientResponse = None

    def __init__(self, client, url, auth_token, verify_ssl):
        self._continue = True
        self._event = {}

        self._client = client
        self._url = url
        self._auth_token = auth_token
        self._verify_ssl = verify_ssl

    def __aiter__(self):
        return self

    async def __anext__(self):
        if not self._continue:
            raise StopAsyncIteration

        if not self._response or self._response.closed:
            await self.start()

        while True:
            try:
                async for line in self._response.content:
                    line = line.decode("utf-8").strip()

                    if not line.strip():
                        event = self._event
                        self._event = {}
                        return event

                    if line.startswith(":"):
                        continue  # It's a comment

                    if ":" not in line:
                        key, value = line, None
                    else:
                        key, value = line.split(":", 1)

                    if key == "data":
                        value = json.loads(value)
                    elif key == "retry":
                        continue  # Retry delay, but we don't need it

                    self._event[key] = value
            except ClientPayloadError:
                if self._continue:
                    log.debug("Connection lost, reconnecting...")
                    await self.start()
                else:
                    raise StopAsyncIteration

    async def start(self):
        self._continue = True

        headers = {
            "X-Auth-Token": self._auth_token
        }

        self._response = await self._client.get(
            self._url, headers=_headers(**headers), verify_ssl=self._verify_ssl
        )

        # If we don't do this, Salt will send a payload that's too big sometimes
        self._response.content._high_water = (4 ** 16) * 2

        log.debug("Connected.")

    def stop(self):
        self._continue = False
        self._response.close()
