"""
SaltStack REST API wrapper module. Provides a class designed for working with
the SaltStack REST API - tested against the rest_cherrypy module.
"""

from datetime import datetime
from logging import getLogger
from typing import Any, Dict, List, Union

from aiohttp import ClientResponse, ClientSession
from cached_property import cached_property

from bot.salt.enums import AuthTypes
from bot.salt.events import EventStream
from bot.salt.exceptions import (
    AuthenticationException, BadRequestException, InternalServerErrorException,
    LoginFailedException, NotFoundException, TimeoutException,
    UnavailableContentTypeException)

log = getLogger(__name__)

HEADERS = {
    "Accept": "application/json",
}


def _headers(**kwargs):
    if kwargs:
        return {**kwargs, **HEADERS}
    return {**HEADERS}


class SaltAPI:
    """
    SaltStack REST API wrapper class.

    This class is tested against the official rest_cherrypy netapi module, using PAM authentication - but LDAP
    and the other REST API modules should work too.
    """

    _client: ClientSession = None

    _username: str
    _password: str
    _url: str
    _ws_url: str
    _auth_type: AuthTypes
    _verify_ssl: bool

    _auth_token: str = None

    permissions: List[Union[str, Dict[str, Union[str, Dict[str, Union[str, Dict[str, Union[List[str], str]]]]]]]] = []

    start_time: datetime = None
    expire_time: datetime = None

    # region: Properties
    @cached_property
    def url_root(self):
        return f"{self._url}/"

    @cached_property
    def url_login(self):
        return f"{self._url}/login"

    @cached_property
    def url_logout(self):
        return f"{self._url}/logout"

    @cached_property
    def url_minions(self):
        return f"{self._url}/minions"

    @cached_property
    def url_jobs(self):
        return f"{self._url}/jobs"

    @cached_property
    def url_run(self):
        return f"{self._url}/run"

    @cached_property
    def url_events(self):
        return f"{self._url}/events"

    @cached_property
    def url_hook(self):
        return f"{self._url}/hook"

    @cached_property
    def url_keys(self):
        return f"{self._url}/keys"

    @cached_property
    def url_ws(self):
        return f"{self._ws_url}/ws"

    @cached_property
    def url_stats(self):
        return f"{self._url}/stats"

    # endregion

    # region: Dunders

    def __init__(
            self, username: str, password: str, url: str,
            auth_type: AuthTypes = AuthTypes.AUTO, verify_ssl: bool = True
    ):
        """
        :param username: The username to authorize with
        :param password: The password to authorize is
        :param url: The base URL for your Salt instance's API
        :param auth_type: The type of authentication to use - set this yourself if you're getting weird auth errors
        :param verify_ssl: Whether to verify SSL certs (recommended)
        """

        if not isinstance(auth_type, AuthTypes):
            raise TypeError(f"auth_type must be a member of the AuthTypes enum, got '{type(auth_type)}'")

        self._username = username
        self._password = password
        self._url = url.rstrip("/")
        self._ws_url = f"ws{url[4:]}"
        self._auth_type = auth_type.value
        self._verify_ssl = verify_ssl

    async def _post(self, url: str, *args, headers: Dict[str, Any] = None, **kwargs) -> ClientResponse:
        if headers is None:
            headers = {}

        headers = _headers(**headers)

        if self._auth_token is not None:
            headers["X-Auth-Token"] = self._auth_token

        if "json" not in kwargs:
            headers["Content-Type"] = headers.get("Content-Type", "application/x-www-form-urlencoded")

        response = await self._client.post(url, *args, headers=headers, verify_ssl=self._verify_ssl, **kwargs)

        log.debug(f"POST {url}: {response.status}")

        if not (url == self.url_login and response.status == 401):
            self._raise_for_status(response.status)  # Throw a different exception if login failed

        return response

    async def _get(self, url: str, *args, headers: Dict[str, Any] = None, **kwargs) -> ClientResponse:
        if headers is None:
            headers = {}

        headers = _headers(**headers)

        if self._auth_token is not None:
            headers["X-Auth-Token"] = self._auth_token

        response = await self._client.get(url, *args, headers=headers, verify_ssl=self._verify_ssl, **kwargs)

        log.debug(f"GET {url}: {response.status}")

        self._raise_for_status(response.status)
        return response

    def _raise_for_status(self, code: int):
        if code == 200:
            return

        if code == 400:
            raise BadRequestException()

        if code == 401:
            raise AuthenticationException()

        if code == 404:
            raise NotFoundException()

        if code == 406:
            raise UnavailableContentTypeException()

        if code == 500:
            raise InternalServerErrorException()

        if code == 504:
            raise TimeoutException()

    # endregion

    async def setup(self):
        """
        Set up the API wrapper by creating a client session. You must call this before logging in.
        """

        self._client = ClientSession()
        log.debug("ClientSession created")

    async def teardown(self):
        """
        Close the client session and delete your credentials. You should always call this when you're finished
        working with this class. If you want to re-use the object, remember to call :code:`setup()` again.
        """

        if self._client is not None:
            if not self._client.closed:
                await self._client.close()

        await self.logout()

        self._client = None

        log.debug("ClientSession closed and destroyed")

    async def login(self):
        """
        Log in with the supplied credentials, and obtain an auth token. You'll need to do this before you try to
        use any other part of the API.
        """

        payload = {
            "eauth": self._auth_type,
            "username": self._username,
            "password": self._password
        }

        response = await self._post(self.url_login, data=payload)

        if response.status != 200:
            raise LoginFailedException()

        data = await response.json()
        data = data.get("return", [{}])[0]

        self._auth_token = data["token"]
        self.permissions = data["perms"]

        self.start_time = datetime.utcfromtimestamp(data["start"])
        self.expire_time = datetime.utcfromtimestamp(data["expire"])

        log.debug(f"Logged in as {self._username}")

    async def logout(self):
        """
        Perform a faux-logout by deleting the stored token and authentication details.
        """

        self._auth_token = None
        self.permissions = []
        self.start_time = None
        self.expire_time = None

        log.debug(f"Logged out")

    async def minions(self, minion_id: str = None) -> List[Dict[str, Any]]:
        """
        Retrieve information on the configured minions under your Salt instance.

        :param minion_id: Optional, the ID of the minion to get information on (omit for all of them)
        """

        url = self.url_minions

        if minion_id is not None:
            url = f"{url}/{minion_id}"

        response = await self._get(url)
        return (await response.json())["return"]

    async def execute_on_minions(self, *, target: str, func: str) -> List[Dict[str, Any]]:
        """
        Execute a function on a minion or set of minions. This method is supplied for the sake of having a complete
        implementation, but you probably want the :code:`run()` method instead.

        :param target: The minion (or glob for multiple minions) to target
        :param func: The function to run
        """

        payload = {
            "tgt": target,
            "fun": func
        }

        response = await self._post(self.url_minions, data=payload)
        return (await response.json())["return"]

    async def jobs(self, job_id: int = None, timeout: int = None) -> List[Dict[str, Any]]:
        """
        Retrieve information on current, most recent or specific jobs.

        :param job_id: Optional, the ID of the job to get information on (omit for all of them)
        :param timeout: Optional, how long Salt should wait before timing out the retrieval
        """

        payload = {}

        if timeout is not None:
            payload["timeout"] = timeout

        url = self.url_jobs

        if job_id is not None:
            url = f"{url}/{job_id}"

        response = await self._get(url, params=payload)
        return (await response.json())["return"]

    async def run(
            self, target: str, func: str, client: str = "local_async", *,
            args: List[str] = None, kwargs: Dict[str, Any] = None
    ) -> List[Dict[str, Any]]:
        """
        Run a function against a target (or multiple targets) - this is probably what you came here for!

        :param target: The target (or target glob) to run the function on
        :param func: The name of the function to run
        :param client: The type of salt client to use - "local" waits for the job, "local_async" returns immediately
        :param args: Arguments to pass to the function
        :param kwargs: Keyword arguments to pass to the function
        """

        payload = {
            "client": client,
            "fun": func,
            "tgt": target,
        }

        if args:
            payload["arg"] = args

        if kwargs:
            payload["kwarg"] = kwargs

        payload = [payload]

        response = await self._post(self.url_root, json=payload)
        return (await response.json())["return"]

    async def apply_state(self, target: str, state: str = None) -> List[Dict[str, Any]]:
        """
        Convenience wrapper around :code:`run()` to apply a state to a target or targets. This will use the
        local_async client type, so it'll return immediately with a job ID object.

        :param target: The target (or target glob) to run the function on
        :param state: Optional: The name of the salt state to apply (omit for the highstate)
        """

        if state is not None:
            return await self.run(target, "state.apply", client="local_async", args=[state])
        else:
            return await self.run(target, "state.apply", client="local_async")

    async def send_hook(self, path: str, **kwargs) -> Dict[str, Any]:
        """
        Send a webhook to Salt's hook system. How you use this will be defined by the hooks that you set up on your
        Salt instance. This will fire an event similar to the following:

        >>> {
        >>>     "tag": f"salt/netapi/hook/{path}",
        >>>     "data": kwargs
        >>> }

        :param path: The webhook path, after "hook/"
        :param kwargs: Keyword arguments to pass to the hook
        """

        path = path.lstrip("/")
        response = await self._post(f"{self.url_hook}/{path}", json=kwargs)
        return await response.json()

    async def get_keys(self, minion_id: str = None) -> List[Dict[str, Any]]:
        """
        Retrieve the list of minion keys or details on the keys for a specific minion.

        :param minion_id: Optional, minion to get the keys for (omit for all of them)
        """

        url = self.url_keys

        if minion_id is not None:
            url = f"{url}/{minion_id}"

        return (await self._get(url))["return"]

    async def generate_keys(self, minion_id: str, key_size: int = 2048, force: bool = False) -> bytes:
        """
        Generate new keys for a minion, and return them as a byte string.

        The key data is contained within a TAR-format archive. It's up to you to take the byte string and write it to
        disk, extract it, or whatever it is that you're doing with it.

        :param minion_id: The minion to generate keys for
        :param key_size: Optional, the key size - defaults to 2048, which is also the minimum allowable size
        :param force: Optional, whether to overwrite the keys for a minion that already has them - defaults to False
        """

        payload = {
            "mid": minion_id,
            "keysize": key_size,
            "force": force,

            "username": self._username,
            "password": self._password,
            "eauth": self._auth_type
        }

        response = await self._post(self.url_keys, data=payload, headers={"Accept": "application/x-tar"})
        return await response.read()

    def events(self) -> EventStream:
        """
        Returns an async iterator over the Salt event bus stream. These events are fired directly by Salt itself.

        >>> api = SaltAPI(...)
        >>> events = api.events()
        >>>
        >>> async for event in events:
        >>>     print(event)
        >>>
        >>> events.stop()

        See the EventStream class for more information on how this works.
        """

        return EventStream(self._client, self.url_events, self._auth_token, self._verify_ssl)

    async def stats(self) -> Dict[str, Any]:
        """
        Get some statistics for the currently-running netapi daemon. This does not provide stats for your Saltstack
        master/minions.
        """

        response = await self._get(self.url_stats)
        return await response.json()
