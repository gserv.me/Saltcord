import logging

from discord.ext.commands import Bot, when_mentioned_or
from logmatic import JsonFormatter

from bot.config import Config

config = Config()
handler = logging.StreamHandler()

if not config.debug:
    handler.setFormatter(JsonFormatter())

logging.basicConfig(
    format="%(asctime)s Saltcord: | %(name)30s | %(levelname)8s | %(message)s",
    datefmt="%b %d %H:%M:%S",
    level=logging.INFO if not config.debug else logging.DEBUG,
    handlers=[handler]
)

if not config.debug:
    logging.getLogger("discord.client").setLevel(logging.ERROR)
    logging.getLogger("discord.gateway").setLevel(logging.ERROR)
    logging.getLogger("discord.state").setLevel(logging.ERROR)
    logging.getLogger("discord.http").setLevel(logging.ERROR)
    logging.getLogger("websockets.protocol").setLevel(logging.ERROR)


log = logging.getLogger(__name__)

bot = Bot(
    command_prefix=when_mentioned_or("!"),
    # activity=Game(name="Commands: !help"),
    case_insensitive=True
)

bot.config = config

bot.load_extension("bot.cogs.salt_commands")
bot.load_extension("bot.cogs.salt_events")
bot.run(config.bot_token)
