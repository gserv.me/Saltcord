import logging

from discord import Colour, Embed
from discord.ext.commands import Bot, Context, command, has_permissions

from bot.pagination import LinePaginator
from bot.salt import SaltAPI
from bot.salt.enums import AuthTypes

log = logging.getLogger(__name__)


class SaltCommands:
    def __init__(self, bot: Bot):
        self.bot = bot
        self.salt = SaltAPI(
            bot.config.salt_username, bot.config.salt_password,
            bot.config.salt_url, AuthTypes.from_string(bot.config.salt_auth_type)
        )

    async def on_ready(self):
        await self.salt.setup()
        await self.salt.login()

    @command()
    @has_permissions(administrator=True)
    async def apply(self, ctx: Context, target: str, *, state: str = None):
        """
        Apply a salt state to a minions. "*" will target all minions.
        """
        result = await self.salt.apply_state(target, state)

        jid = result[0]["jid"]
        lines = []

        async with ctx.typing():
            while True:
                job_status = (await self.salt.jobs(jid))[0]

                log.debug(job_status)

                if job_status:
                    if len(job_status) == 1 and isinstance(list(job_status.values())[0], list):
                        errors = list(job_status.values())[0]

                        embed = Embed(title=f"Failed to apply states: {target}")
                        embed.colour = Colour.red()
                        embed.description = "\n".join(errors)

                        await ctx.send(embed=embed)
                        return

                    embed = Embed(title=f"States applied: {target}")
                    colour = Colour.green()

                    result_sets = {}

                    for minion, data in job_status.items():
                        result_sets[minion] = []

                        for _, result in data.items():
                            if not result["result"]:
                                colour = Colour.gold()

                            line = (
                                f"**`[{result['__sls__']}]`** ({result['__id__']}) -> "
                                f"**{result['result']}**\n{result['comment']}"
                            )

                            result_sets[minion].append(line)

                    embed.colour = colour

                    for _, line_set in result_sets.items():
                        lines += line_set

                    break
        if lines:
            await LinePaginator.paginate(lines, ctx, embed)


def setup(bot):
    bot.add_cog(SaltCommands(bot))
    log.info("Cog loaded: SaltCommands")
