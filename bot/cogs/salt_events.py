import logging
from datetime import datetime

from discord import Colour, Embed
from discord.ext.commands import Bot, Context

from bot.salt import SaltAPI
from bot.salt.enums import AuthTypes
from bot.salt.events import EventStream

log = logging.getLogger(__name__)

FUNCTION_BLACKLIST = (
    "runner.jobs.list_job",
)


class FakeContext(Context):
    def __init__(self, channel):
        self._channel = channel

    @property
    def channel(self):
        return self._channel


class SaltEvents:
    events: EventStream = None

    def __init__(self, bot: Bot):
        self.bot = bot
        self.salt = SaltAPI(
            bot.config.salt_username, bot.config.salt_password,
            bot.config.salt_url, AuthTypes.from_string(bot.config.salt_auth_type)
        )

    def __unload(self):
        if self.events is not None:
            self.events.stop()

    async def on_ready(self):
        await self.salt.setup()
        await self.salt.login()

        self.events = self.salt.events()
        channel = self.bot.get_channel(self.bot.config.updates_channel)  # TODO: Configurable

        async for event in self.events:
            tag = event.get("tag", "")

            log.debug(f"{tag} -> {event}")

            if "/" not in tag:
                continue

            parts = tag.split("/")

            if parts[1] == "job":
                job_id = parts[2]

                if len(parts) > 3:
                    if parts[3] == "new":  # Job created
                        data = event["data"]["data"]  # Yeah, I know. Don't @ me. I have no fucking idea.
                        func = data.get("fun")

                        if func in FUNCTION_BLACKLIST:
                            continue

                        if func == "state.apply":
                            state = data["arg"][0]

                            embed = Embed(title=f"State running: {state}", color=Colour.blurple())

                            embed.set_footer(text=f"ID: {job_id}")
                            embed.timestamp = datetime.now()

                            await channel.send(embed=embed)
                        # else:
                        #     args = ",\n    ".join([repr(x) for x in data.get("arg", [])])
                        #     target = data.get("tgt")
                        #
                        #     embed = Embed(title=f"Job created: {target}", color=Colour.blurple())
                        #     embed.set_footer(text=f"ID: {job_id}")
                        #
                        #     embed.timestamp = datetime.now()
                        #
                        #     if args:
                        #         embed.description = f"```py\n{func}(\n    {args}\n)\n```\n"
                        #     else:
                        #         embed.description = f"```py\n{func}()\n```\n"
                        #
                        #     await channel.send(embed=embed)
                    elif parts[3] == "ret":  # Job complete
                        data = event["data"]["data"]
                        func = data.get("fun")

                        if func in FUNCTION_BLACKLIST:
                            continue

                        if func == "state.apply":
                            state = data["fun_args"][0]
                            success = data.get("success")

                            if success:
                                embed = Embed(title=f"State succeeded: {state}", color=Colour.green())
                            else:
                                embed = Embed(title=f"State failed: {state}", color=Colour.red())

                            embed.set_footer(text=f"ID: {job_id}")
                            embed.timestamp = datetime.now()

                            await channel.send(embed=embed)
                        # else:
                        #     args = ",\n    ".join([repr(x) for x in data.get("fun_args", [])])
                        #     target = data.get("id")
                        #     success = data.get("success", False)
                        #
                        #     if success:
                        #         embed = Embed(title=f"Job succeeded: {target}", color=Colour.green())
                        #     else:
                        #         embed = Embed(title=f"Job failed: {target}", color=Colour.red())
                        #
                        #     embed.set_footer(text=f"ID: {job_id}")
                        #
                        #     embed.timestamp = datetime.now()
                        #
                        #     if args:
                        #         embed.description = f"```py\n{func}(\n    {args}\n)\n```\n"
                        #     else:
                        #         embed.description = f"```py\n{func}()\n```\n"
                        #
                        #     await channel.send(embed=embed)

            # event_str = str(event)
            #
            # if len(event_str) > 1900:
            #     event_str = event_str[:1900]
            #
            # await channel.send(f"```json\n{event_str}\n```")


def setup(bot):
    bot.add_cog(SaltEvents(bot))
    log.info("Cog loaded: SaltEvents")
