import os

from cached_property import cached_property


class Config:
    def __init__(self):
        if not self.bot_token:
            raise ValueError("Must supply the BOT_TOKEN environment variable.")
        if not self.salt_password:
            raise ValueError("Must supply the SALT_PASSWORD environment variable.")
        if not self.salt_url:
            raise ValueError("Must supply the SALT_URL environment variable.")
        if not self.salt_username:
            raise ValueError("Must supply the SALT_USERNAME environment variable.")
        if not self.updates_channel:
            raise ValueError("Must supply the UPDATES_CHANNEL environment variable.")

    @cached_property
    def bot_token(self):
        return os.getenv("BOT_TOKEN", None)

    @cached_property
    def debug(self):
        return "DEBUG" in os.environ

    @cached_property
    def salt_auth_type(self):
        return os.getenv("SALT_AUTH_TYPE", "pam")

    @cached_property
    def salt_password(self):
        return os.getenv("SALT_PASSWORD", None)

    @cached_property
    def salt_url(self):
        return os.getenv("SALT_URL", "")

    @cached_property
    def salt_username(self):
        return os.getenv("SALT_USERNAME", None)

    @cached_property
    def updates_channel(self):
        return int(os.getenv("UPDATES_CHANNEL", "0"))
